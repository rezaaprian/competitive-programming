package com.aprionstudio.algods.basic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class JavaKeyword {

    public static void main(String[] args) {

        //initArray
        int[] num = new int[]{1, 2, 3};
        int[] num2 = new int[5];
        int[][] twoDimensArr = {{2, 7, 9}, {3, 6, 1}, {7, 4, 2}};
        // 2 7 9    [x,y]   [x,y+1]   [x,y+2]
        // 3 6 1    [x+1,y] [x+1,y+1] [x+1,y+2]
        // 7 4 2    [x+2,y] [x+2,y+1] [x+2, y+2]
        int height = twoDimensArr.length;
        int width = twoDimensArr[0].length;

        String name = "reza";
        char nameChar[] = name.toCharArray();
        for (char c : nameChar) {
            //looping array
        }

        //char array to String
        String toSt = new String(nameChar);
        String toStSubs = new String(nameChar,0,2); //start at 0 | length 2

        //substring
        String eza = name.substring(1);
        String ez = name.substring(1,3); //begin inclusive, end exclusive (length = end - begin)
        String e = name.substring(1,1);

        //queue (like in bank, FIFO)
        Queue<Character> queue = new LinkedList<>();
        queue.add('a');
        queue.add('b');
        char removed = queue.remove(); //a

        //stacks (FILO)
        Stack<Character> stack = new Stack<>();
        stack.push('a');
        stack.push('b');
        char popped = stack.pop(); //b

        //Sorting a List
        List<Integer> numbers = new ArrayList<>();
        Collections.sort(numbers);

        //Sorting an Array
        Arrays.sort(num);

        //IndexOfArray (we can find index on sorted list with binary search)
        Arrays.sort(num);
        int index = Arrays.binarySearch(num, 1);

        //IndexOfList
        numbers.indexOf(1); //first occurence
        numbers.lastIndexOf(1); //last occurence

        //Factorial
        int fact = 1;
        int number = 5;
        for (int i = 1; i <= number; i++) {
            fact = fact * i;
        }
        // with recursion means return (n * factorial(n-1))
    }

    //Fibonacci
    public static int fibo(int n) {
        // Base Case
        if (n <= 1)
            return n;
        
        // Recursive call
        return fibo(n - 1) + fibo(n - 2);
    }
}
