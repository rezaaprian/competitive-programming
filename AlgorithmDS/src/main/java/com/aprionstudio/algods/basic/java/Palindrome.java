package com.aprionstudio.algods.basic.java;

public class Palindrome {

    public static void main(String[] args) {
        int num = 9752;
        int numReversed = reverseNumber(num);
        System.out.println(numReversed);
    }

    public static int reverseNumber(int x) {
        String reversed = new StringBuilder().append(Math.abs(x)).reverse().toString();
        try {
            return (x < 0) ? Integer.parseInt(reversed) * -1 : Integer.parseInt(reversed);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
