package com.aprionstudio.algods.basic

object Sorting {

    @JvmStatic
    fun main(args: Array<String>) {
        val arr = intArrayOf(1, 2, 2, 2, 3, 4, 6, 7, 8, 9, 10)
//        printArray(arr)
//        bubbleSort(arr)
//        printArray(arr)
        println(binarySearch(arr, 0, arr.size - 1, 3))
    }

    private fun printArray(arr: IntArray) {
        for (i in arr.indices) {
            print(arr[i])
        }
        print("\n")
    }

    fun swapElementInArray(i: Int, j: Int, arr: IntArray) {
        println("swap $i $j")
        if (i != j) {
            arr[i] = arr[i] + arr[j]
            arr[j] = arr[i] - arr[j]
            arr[i] = arr[i] - arr[j]
        }
        printArray(arr)
    }

    /**
     * Binary search is to find element index in a presorted array
     * it will divide an array and check half of it
     */
    private fun binarySearch(arr: IntArray, left: Int, right: Int, find: Int): Int {
        if (arr.isEmpty())
            return -1
        if (right >= left) {
            //get the middle index
            val mid = left + (right - 1) / 2
            //if middle index have the value return it
            if (arr[mid] == find)
                return mid
            //else if middle index value still bigger means the value is on the left, do binary search again on the left
            if (arr[mid] > find)
                return binarySearch(arr, left, mid - 1, find)
            // else do binary search to the right
            return binarySearch(arr, mid + 1, right, find)
        }
        return -1
    }

    /**
     * The simplest sorting algorithm that take double for loops
     * move the highest to the right most and repeat
     * (repeat with smaller area (size - n))
     */
    private fun bubbleSort(arr: IntArray) {
        for (i in arr.indices) {
            for (j in 0 until arr.size - i - 1) {
                if (arr[j] > arr[j + 1]) {
                    swapElementInArray(j, j + 1, arr)
                }
            }
        }
    }

    /**
     * Insertion sort is sorting algorithm good for smaller collection of data, or mostly sorted one
     * istilahnya menggunakan sortedTempArray dari kiri
     * kemudian masukan value sebelah kanannya satu satu
     */
    private fun insertionSort(arr: IntArray) {
        //looping dari i=1 karena 0 akan menjadi "sortedTempArray"
        for (i in 1 until arr.size) {
            var j = i - 1
            while (j >= 0 && arr[j] > arr[j + 1]) {
                //swap element and move further left
                swapElementInArray(j, j + 1, arr)
                j--
            }
        }
    }

    /**
     * Almost the same with bubble sort, but find the minimum index and swap with first position
     * move to the right
     */
    private fun selectionSort(arr: IntArray) {
        selectionSortRecursive(arr, 0)
    }

    private fun selectionSortRecursive(arr: IntArray, start: Int) {
        //cari minimum index dari start - end, swap dengan posisi start, lanjutkan ke posisi berikutnya
        if (start < arr.size - 1) {
            swapElementInArray(start, findMinimumIndex(arr, start), arr)
            selectionSortRecursive(arr, start + 1)
        }
    }

    private fun findMinimumIndex(arr: IntArray, start: Int): Int {
        if (arr.isEmpty())
            return 0;
        var min = start
        for (i in start until arr.size) {
            if (arr[i] < arr[min]) {
                min = i
            }
        }
        return min
    }
}
