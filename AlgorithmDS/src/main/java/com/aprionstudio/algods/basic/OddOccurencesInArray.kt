package com.aprionstudio.algods.basic

object OddOccurencesInArray {

    /*
    Problem Statement: Find an odd occurence (one without pair) in array, there will be only one element inside an array with no pairings
     */

    @JvmStatic
    fun main(args: Array<String>) {
        val arr = intArrayOf(1,1,2,2,3,3,4,5,5,5,5)
//        println("xor 1 xor 2 = " + (1 xor 2))
        println(findOddWithXor(arr))
    }

    fun findOddWithXor(A: IntArray): Int  {
        /*
        Properties of XOR
        (1) n ^ 0 = n
        (2) n ^ n = 0
        (3) n ^ m = n + m
         */
        var xors = 0
        for(i in A.indices) {
            xors = xors xor A[i]
        }
        return xors
    }

    // here we loop for each number once (save previous n for optimization) if the count is even / odd
    fun findOdd(A: IntArray): Int {
        // write your code in Kotlin
        var current = A[0]
        var count : Int
        val done : MutableList<Int> = emptyList<Int>().toMutableList()
        for(i in A.indices) {
            if(done.contains(A[i]))
                continue
            current = A[i]
            done.add(current)
            count = 1
            for(j in i+1..A.size-1) {
                if(A[j] == current) {
                    count++
                    // println("find= " + current + " index= " + j + " count= " + count)
                }
            }
            if(count%2!=0) {
                //  println("current= " + current + " count= " + count)
                return current
            }
        }
        return 0
    }

}