package com.aprionstudio.algods.basic

import java.util.*

object ReverseString {

    @JvmStatic
    fun main(args: Array<String>) {
        val word = "terbalik"
        val reversed = word.reversed()
        val arr = intArrayOf(1, 2, 5, 4, 3)
        Arrays.sort(arr)
        for (number in arr) {
            print(number)
        }
        println(reversed)
    }
}