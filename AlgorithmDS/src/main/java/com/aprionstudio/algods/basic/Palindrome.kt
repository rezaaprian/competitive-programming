package com.aprionstudio.algods.basic

object Palindrome {

    @JvmStatic
    fun main(args: Array<String>) {
        val num = 123123
        val word = num.toString()
        println(word == word.reversed())
    }

    private fun isPalindrome(word: String): Boolean {
        if (word.isEmpty() || word.length % 2 != 0)
            return false
        var l = 0
        var r = word.length - 1
        while (l < r) {
            if (word[l] != word[r])
                return false
            l++
            r--
        }
        return true
    }

    private fun isPalindrome2(word: String): Boolean {
        val reverseWord = word.reversed()
        return word == reverseWord
    }
}