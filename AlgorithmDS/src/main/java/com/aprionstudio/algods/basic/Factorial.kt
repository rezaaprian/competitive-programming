package com.aprionstudio.algods.basic

object Factorial {

    @JvmStatic
    fun main(args: Array<String>) {
        println(factorialRecursive(4))
        println(factorialDP(4))
    }

    //recursive
    fun factorialRecursive(n: Int) : Int {
        return if(n<2) return n else n * factorialRecursive(n-1)
    }

    //dynamic programming
    fun factorialDP(n: Int) : Int {
        var result = 1
        for (i in 1..n) {
            result *= i
        }
        return result
    }
}