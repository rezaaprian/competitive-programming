package com.aprionstudio.algods.basic.java;

public class TwoSumSorted {

    public static void main(String[] args) {
        int[] num = new int[]{1, 2, 3, 4, 5};
        int[] result = twoSum(num, 7);
        System.out.println(result[0] + " and " + result[1]);
    }

    public static int[] twoSum(int[] nums, int target) {
        int left = 0;
        int right = nums.length-1;

        while(left<right) {
            if(nums[left] + nums[right] == target) {
                return new int[] {left +1,right +1};
            } else if(nums[left] + nums[right] < target) {
                // harusnya lebih besar geser ke kanan
                left += 1;
            } else {
                //harusnya lebih kecil geser ke kiri
                right -= 1;
            }
        }
        return new int[] {0,0};
    }
}
