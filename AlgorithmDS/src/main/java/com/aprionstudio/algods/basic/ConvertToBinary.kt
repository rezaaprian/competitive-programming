package com.aprionstudio.algods.basic

object ConvertToBinary {

    @JvmStatic
    fun main(args: Array<String>) {
        val binary: MutableList<Int> = emptyList<Int>().toMutableList()
        var n = 32
        while (n > 0) {
            binary.add(n % 2)
            n /= 2
        }
        binary.reverse()
        for(i in binary.indices) {
            print(binary[i])
        }
        println()

        // binary-gap (longest)
        var first = 0
        var current = 0
        var longest = 0
        for(i in binary.indices) {
            if(binary[i] == 1) {
                current = i-first
                first = i + 1
                if(longest<current) {
                    longest = current
                }
            }
        }
        println(longest)
    }
}
