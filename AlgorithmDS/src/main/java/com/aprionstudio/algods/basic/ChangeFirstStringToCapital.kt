package com.aprionstudio.algods.basic

import java.util.*
import java.util.regex.Pattern

object ChangeFirstStringToCapital {

    @JvmStatic
    fun main(args: Array<String>) {
        val words = "Serafi12na34"
        val sentences = "my name is reza"
        println(sentences)
        println(capitalizeWordInString(sentences))
    }

    fun capitalize(word: String): String {
        val char = word.toCharArray()
        if (char.isNotEmpty())
            char[0] = char[0].toUpperCase()
        return char.joinToString("")
    }

    fun capitalizeWithSubstring(word: String): String {
        if (word.isEmpty())
            return ""
        return word.substring(0, 1).toUpperCase(Locale.ROOT) + word.substring(1)
    }

    fun removeNumberFromString(word: String): String {
        val pattern = Pattern.compile("[0-9]")
        return pattern.matcher(word).replaceAll("")
    }

    fun capitalizeWordInString(sentence: String): String {
        val words = sentence.split(" ")
        var newSentence = ""
        for (word in words) {
            println(word)
            newSentence += word.substring(0, 1).toUpperCase(Locale.ROOT) + word.substring(1) + " "
        }
        return newSentence.substring(0, newSentence.length - 1)
    }
}
