package com.aprionstudio.algods.basic

import java.util.*

object KotlinKeyword {

    @JvmStatic
    fun main(args: Array<String>) {

        // initArray
        val num = intArrayOf(1, 2, 3)
        num.forEach { print(it) }
        for (item in num) print(item)

        for (i in 1..3) { // 1<= i <= 3 all included
            println(i)
        }

        for (i in 1 until 3) { // 3 excluded
            println(i)
        }

        for (i in 6 downTo 0 step 2) {
            println(i)
        }
        // empty size 5
        val num2 = IntArray(5)
        val twoDimensArr = arrayOf(intArrayOf(2, 7, 9), intArrayOf(3, 6, 1), intArrayOf(7, 4, 2))
        // 2 7 9    [x,y]   [x,y+1]   [x,y+2]
        // 3 6 1    [x+1,y] [x+1,y+1] [x+1,y+2]
        // 7 4 2    [x+2,y] [x+2,y+1] [x+2, y+2]
        // 2 7 9    [x,y]   [x,y+1]   [x,y+2]
        // 3 6 1    [x+1,y] [x+1,y+1] [x+1,y+2]
        // 7 4 2    [x+2,y] [x+2,y+1] [x+2, y+2]
        val height = twoDimensArr.size
        val width: Int = twoDimensArr[0].size

        // init List
        var listEmpty = emptyList<Int>().toMutableList()

        val name = "reza"
        val nameChar = name.toCharArray()
        for (c in nameChar) {
            //looping array
        }

        //char array to String
        val toSt = String(nameChar)
        val toStSubs = String(nameChar, 0, 2) //start at 0 | length 2

        //substring
        val eza = name.substring(1)
        val ez = name.substring(1, 3) //begin inclusive, end exclusive (length = end - begin)
        val e = name.substring(1, 1)

        //queue (like in bank, FIFO)
        val queue: Queue<Char> = LinkedList()
        queue.add('a')
        queue.add('b')
        val removed = queue.remove() //a

        //stacks (FILO)
        val stack = Stack<Char>()
        stack.push('a')
        stack.push('b')
        val popped = stack.pop() //b

        //Sorting a List
        val numbers: List<Int> = ArrayList()
        Collections.sort(numbers)

        //Sorting an Array
        Arrays.sort(num)

        //IndexOfArray (we can find index on sorted list with binary search)
        Arrays.sort(num)
        val index = Arrays.binarySearch(num, 1)

        //IndexOfList
        numbers.indexOf(1) //first occurence

        numbers.lastIndexOf(1) //last occurence
    }
}