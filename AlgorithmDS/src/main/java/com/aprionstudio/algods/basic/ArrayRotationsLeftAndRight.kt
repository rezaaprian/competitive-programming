package com.aprionstudio.algods.basic

object ArrayRotationsLeftAndRight {

    /*
   Problem Statement: Rotate an array K-times to the left or right
    */

    @JvmStatic
    fun main(args: Array<String>) {
        val arr = intArrayOf(1, 2, 3)
        printArray(rotateLeft(arr, 1000))
        println()
        printArray(rotateRight(arr, 1000))
    }

    fun printArray(A: IntArray) {
        for (i in A.indices) {
            print(A[i])
        }
    }

    fun rotateLeft(A: IntArray, K: Int): IntArray {
        if (A.size == 1)
            return A
        val B = A.copyOf(A.size)
        for (i in A.indices) {
            // cycle left
            B[i] = A[((i + K) % A.size)]
        }
        return B
    }

    fun rotateRight(A: IntArray, K: Int): IntArray {
        // base case
        if (A.size < 2)
            return A

        val B = A.copyOf(A.size)
        var L = K

        // case if size larger than K
        if (K > A.size) //if size 2, K = 3 rotation mean K = 1 rotation
            L %= A.size

        for (i in A.indices) {
            // cycle right
            if (i < L) {
                B[i] = A[i - L + A.size]
            } else {
                B[i] = A[i - L]
            }

        }
        return B
    }
}