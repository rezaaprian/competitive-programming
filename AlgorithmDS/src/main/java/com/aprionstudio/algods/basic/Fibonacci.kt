package com.aprionstudio.algods.basic

object Fibonacci {

    // Recursive
    fun fibonacciRecursiveNth(n: Int): Int {
        return if(n<2) n else fibonacciRecursiveNth(n-1) + fibonacciRecursiveNth(n-2)
    }

    // DP with memo
    fun fibonacciNthDP(n: Int): Int {
        val memo = IntArray(n+2) // extra number to handle n = 0
        memo[0] = 0
        memo[1] = 1

        for(i in 2..n) {
            memo[i] = memo[i-1] + memo[i-2]
        }

        return memo[n]
    }

    @JvmStatic
    fun main(args: Array<String>) {
        println(fibonacciRecursiveNth(1))
        println(fibonacciRecursiveNth(2))
        println(fibonacciRecursiveNth(3))
        println(fibonacciRecursiveNth(4))
        println(fibonacciRecursiveNth(5))
        println(fibonacciRecursiveNth(6))
        println(fibonacciRecursiveNth(7))
        println(fibonacciRecursiveNth(8))
        println(fibonacciRecursiveNth(9))
        println(fibonacciNthDP(1))
        println(fibonacciNthDP(2))
        println(fibonacciNthDP(3))
        println(fibonacciNthDP(4))
        println(fibonacciNthDP(5))
        println(fibonacciNthDP(6))
        println(fibonacciNthDP(7))
        println(fibonacciNthDP(8))
        println(fibonacciNthDP(9))
    }
}