package com.aprionstudio.algods.basic

import kotlin.Comparator
import kotlin.collections.ArrayList

object SortingListCustom {

    @JvmStatic
    fun main(args: Array<String>) {
        val ar: ArrayList<Student> = ArrayList<Student>()
        ar.add(Student(111, "bbbb", "london"))
        ar.add(Student(131, "aaaa", "nyc"))
        ar.add(Student(121, "cccc", "jaipur"))
        printArray(ar)
        ar.sortWith(SortByRoll())
        printArray(ar)

    }

    fun printArray(students: ArrayList<Student>) {
        for(student in students) {
            println(student.s)
        }
    }

    class SortByRoll : Comparator<Student>  {
        override fun compare(p0: Student?, p1: Student?): Int {
            //sorting number
//            return p0?.i!! - p1?.i!!
            //sorting words
            return p0?.s!!.compareTo(p1?.s!!)
        }

    }

    class Student(var i: Int, var s: String, var s1: String) {

    }


}