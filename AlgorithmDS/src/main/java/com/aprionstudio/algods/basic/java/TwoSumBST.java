package com.aprionstudio.algods.basic.java;

import java.util.ArrayList;
import java.util.List;

public class TwoSumBST {

    public static void main(String[] args) {
//        System.out.println(preorderTraversal(root, k, new ArrayList<Integer>()));
    }

    public static boolean preorderTraversal(TreeNode root, int k, List<Integer> list) {
        if (root == null) return false;
        //do search here
        int searched = k - root.val;
        if (list.contains(searched)) {
            return true;
        }
        list.add(root.val);
        return preorderTraversal(root.left, k, list) || preorderTraversal(root.right, k, list);
    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

}
