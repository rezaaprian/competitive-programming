package com.aprionstudio.algods.basic

object PowerWithRecursive {

    @JvmStatic
    fun main(args: Array<String>) {
        val n = 2
        val pow = 5
        println(powerWithRecursive(n, pow))
    }

    private fun powerWithRecursive(n: Int, pow: Int): Int {
        if (pow == 1) return n
        return n * powerWithRecursive(n, pow - 1)
    }
}
