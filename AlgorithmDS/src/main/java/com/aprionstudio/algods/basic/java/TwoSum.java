package com.aprionstudio.algods.basic.java;

import java.util.HashMap;

public class TwoSum {

    public static void main(String[] args) {
        int[] num = new int[]{1, 2, 3, 4, 5};
        int[] result = twoSum(num, 7);
        System.out.println(result[0] + " and " + result[1]);
    }

    public static int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        return new int[]{0, 0};
    }

    public static int[] twoSumHashMap(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; i++) {
            int searched = target - nums[i];
            if (map.containsKey(searched)) {
                return new int[]{map.get(searched), i};
            }
            map.put(nums[i], i);
        }
        return new int[]{0, 0};
    }
}
